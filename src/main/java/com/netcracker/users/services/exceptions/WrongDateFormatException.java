package com.netcracker.users.services.exceptions;

public class WrongDateFormatException extends Exception {
    public WrongDateFormatException(String message) {
        super(message);
    }
}
