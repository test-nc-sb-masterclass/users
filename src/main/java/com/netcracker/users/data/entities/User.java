package com.netcracker.users.data.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account_login")
    private String login;
    @Column(name = "account_password")
    private String password;

    @OneToOne(mappedBy = "user")
    private UserData data;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdWhen;

    private Long createdByUserId;
}
