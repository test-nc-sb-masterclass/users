package com.netcracker.users.data.dtos;

import com.netcracker.users.data.entities.User;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;

@Getter
@Setter
public class UserDto {

    Long userId;
    String login;
    String password;
    String firstName;
    String lastName;
    String surname;
    String chatNickname;
    @Schema(description = "Format: yyyy-MM-dd", example = "2021-08-20")
    String createdWhen;
    Long createdByUserId;

    public static UserDto toModel(User user) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        UserDto dto = new UserDto();
        dto.userId = user.getId();
        if (user.getData() == null) {
            return dto;
        }
        dto.firstName = user.getData().getFirstName();
        dto.lastName = user.getData().getLastName();
        dto.surname = user.getData().getSurname();
        dto.chatNickname = user.getData().getChatNickname();

        if (user.getData().getCreatedWhen() != null) {
            dto.createdWhen = format.format(user.getCreatedWhen());
        }
        dto.createdByUserId = user.getCreatedByUserId();
        return dto;
    }
}
