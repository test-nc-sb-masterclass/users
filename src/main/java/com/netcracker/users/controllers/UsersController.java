package com.netcracker.users.controllers;

import com.netcracker.users.data.dtos.UserDto;
import com.netcracker.users.data.entities.User;
import com.netcracker.users.data.repositories.UsersRepository;
import com.netcracker.users.services.UsersService;
import com.netcracker.users.services.exceptions.ItemNotFoundException;
import com.netcracker.users.services.exceptions.WrongDateFormatException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.netcracker.users.data.dtos.UserDto.toModel;

@RestController
public class UsersController {

    private final UsersRepository usersRepository;

    private final UsersService usersService;

    public UsersController(UsersRepository usersRepository, UsersService usersService) {
        this.usersRepository = usersRepository;
        this.usersService = usersService;
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserDto> updateUserById(@PathVariable Long id, @RequestBody UserDto userDto) {
        try {
            User user = usersService.updateUser(id, userDto);
            return ResponseEntity.ok(toModel(user));
        } catch (ItemNotFoundException | WrongDateFormatException e) {
            return ResponseEntity.badRequest().body(null);
        }
    }
}
