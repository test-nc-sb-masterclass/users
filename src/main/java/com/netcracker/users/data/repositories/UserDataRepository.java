package com.netcracker.users.data.repositories;

import com.netcracker.users.data.entities.UserData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDataRepository extends JpaRepository<UserData, Long> {
}
