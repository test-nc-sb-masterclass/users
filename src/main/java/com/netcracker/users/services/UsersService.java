package com.netcracker.users.services;

import com.netcracker.users.data.dtos.UserDto;
import com.netcracker.users.data.entities.User;
import com.netcracker.users.data.entities.UserData;
import com.netcracker.users.data.repositories.UserDataRepository;
import com.netcracker.users.data.repositories.UsersRepository;
import com.netcracker.users.services.exceptions.ItemNotFoundException;
import com.netcracker.users.services.exceptions.WrongDateFormatException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class UsersService {
    private final UsersRepository usersRepository;
    private final UserDataRepository userDataRepository;

    public UsersService(UsersRepository usersRepository, UserDataRepository userDataRepository) {
        this.usersRepository = usersRepository;
        this.userDataRepository = userDataRepository;
    }

    @Transactional
    public User updateUser(Long userId, UserDto userDto)
            throws ItemNotFoundException, WrongDateFormatException {
        Optional<User> userOptional = usersRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new ItemNotFoundException("No such user");
        }
        User user = userOptional.get();
        updateUserData(user, userDto);
        return usersRepository.save(user);
    }

    private void updateUserData(User user, UserDto userDto) throws WrongDateFormatException {
        UserData userData = user.getData();
        if (userData == null) {
            userData = new UserData();
            userData.setUserId(user.getId());
            userData.setCreatedWhen(new Date());
        }
        if (userDto.getFirstName() != null) {
            userData.setFirstName(userDto.getFirstName());
        }
        if (userDto.getLastName() != null) {
            userData.setLastName(userDto.getLastName());
        }
        if (userDto.getSurname() != null) {
            userData.setSurname(userDto.getSurname());
        }
        if (userDto.getChatNickname() != null) {
            userData.setChatNickname(userDto.getChatNickname());
        }
        userDataRepository.save(userData);
    }
}
