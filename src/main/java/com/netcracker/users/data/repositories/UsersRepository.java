package com.netcracker.users.data.repositories;

import com.netcracker.users.data.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Long> {
}
