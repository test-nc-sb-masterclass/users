package com.netcracker.users.data.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_data")
@Getter
@Setter
public class UserData {
    @Id
    @Column(name = "user_id")
    private Long userId;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    private String firstName;

    private String lastName;

    private String surname;

    private String chatNickname;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_when")
    private Date createdWhen;

    private Long createdByUserId;
}
